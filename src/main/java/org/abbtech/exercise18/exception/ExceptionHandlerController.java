package org.abbtech.exercise18.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(value = {ArithmeticException.class})
    public ResponseEntity<ErrorResponse> handleArithmeticException(HttpServletRequest request,
                                                                   ArithmeticException exception){
        ErrorResponse errorResponse = new ErrorResponse(LocalDateTime.now().toString(),400,
                exception.getMessage(), request.getServletPath());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

}
