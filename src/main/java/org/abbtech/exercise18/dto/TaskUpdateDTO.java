package org.abbtech.exercise18.dto;


import org.abbtech.exercise18.enums.TaskStatus;

public record TaskUpdateDTO(int id, String title, String desc, TaskStatus status) { }
