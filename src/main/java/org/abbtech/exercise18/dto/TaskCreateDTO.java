package org.abbtech.exercise18.dto;


import org.abbtech.exercise18.enums.TaskStatus;

import java.sql.Timestamp;

public record TaskCreateDTO(String title, String desc, Timestamp created_at, Timestamp updated_at, TaskStatus status) { }
