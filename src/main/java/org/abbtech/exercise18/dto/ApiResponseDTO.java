package org.abbtech.exercise18.dto;

public record ApiResponseDTO<T>(boolean success, String message, T data) { }
