package org.abbtech.exercise18.dto;

public record ApiErrorResponseDTO(boolean success, String message) { }
