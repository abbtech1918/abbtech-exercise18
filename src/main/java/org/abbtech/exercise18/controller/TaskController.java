package org.abbtech.exercise18.controller;

import org.abbtech.exercise18.dto.ApiErrorResponseDTO;
import org.abbtech.exercise18.dto.ApiResponseDTO;
import org.abbtech.exercise18.dto.TaskCreateDTO;
import org.abbtech.exercise18.dto.TaskResultDTO;
import org.abbtech.exercise18.enums.TaskStatus;
import org.abbtech.exercise18.repository.TaskRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {
     TaskRepository taskRepository;
    public TaskController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }



    @GetMapping
    public ResponseEntity<?> getAllTasks(){
        return ResponseEntity.ok(new ApiResponseDTO<>(true, "Task fetched successfully", taskRepository.getAllTasks()));
    }

    @PostMapping
    public ResponseEntity<?> saveTask(@RequestBody TaskCreateDTO taskCreateDTO){
        taskCreateDTO =new TaskCreateDTO(taskCreateDTO.title(),
                taskCreateDTO.desc(),
                Timestamp.valueOf(LocalDateTime.now()),
                Timestamp.valueOf(LocalDateTime.now()),
                TaskStatus.PROCESSING);
        return ResponseEntity.ok(new ApiResponseDTO<>(true, "Task created successfully", taskRepository.createTask(taskCreateDTO)));
    }
    @DeleteMapping("/{taskId}")
    public ResponseEntity<?> deleteTaskByID(@PathVariable Integer taskId){
        if (taskRepository.deleteTaskByID(taskId)){
            return ResponseEntity.ok(new ApiResponseDTO<>(true, "Task deleted successfully", null));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiErrorResponseDTO(false,"Task not delete successfully"));
    }
}
