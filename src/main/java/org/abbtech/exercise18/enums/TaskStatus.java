package org.abbtech.exercise18.enums;

public enum TaskStatus {
    PROCESSING("processing"),
    COMPLETED("completed"),
    DELETED("deleted");

    private final String value;

    TaskStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static TaskStatus fromValue(String value) {
        for (TaskStatus status : values()) {
            if (status.getValue().equalsIgnoreCase(value)) {
                return status;
            }
        }
        throw new IllegalArgumentException("Unknown value: " + value);
    }
}
