package org.abbtech.exercise18.interfaces;

import org.abbtech.exercise18.dto.TaskResultDTO;
import org.abbtech.exercise18.dto.TaskCreateDTO;
import org.abbtech.exercise18.dto.TaskUpdateDTO;

import java.util.List;

public interface ITaskRepository {
    List<TaskResultDTO> getAllTasks();

    boolean deleteTaskByID(int id);

    TaskResultDTO createTask(TaskCreateDTO taskCreateDTO);
    TaskResultDTO updateTask(TaskUpdateDTO taskUpdateDTO);
}
