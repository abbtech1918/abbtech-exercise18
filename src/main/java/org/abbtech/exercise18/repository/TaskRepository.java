package org.abbtech.exercise18.repository;

import org.abbtech.exercise18.dto.TaskResultDTO;
import org.abbtech.exercise18.dto.TaskCreateDTO;
import org.abbtech.exercise18.dto.TaskUpdateDTO;
import org.abbtech.exercise18.enums.TaskStatus;
import org.abbtech.exercise18.interfaces.ITaskRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import javax.sql.RowSet;
import java.sql.ResultSet;
import java.util.List;

@Repository
public class TaskRepository implements ITaskRepository {
    private final JdbcTemplate jdbcTemplate;

    public TaskRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<TaskResultDTO> getAllTasks() {
        return jdbcTemplate.query("SELECT * FROM public.tasks",
                ((rs, rowNum) -> new TaskResultDTO(rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("desc"),
                        rs.getTimestamp("created_at"),
                        rs.getTimestamp("updated_at"),
                        TaskStatus.fromValue(rs.getString("status")))));
    }

    @Override
    public boolean deleteTaskByID(int id) {
        int rowsAffected = jdbcTemplate.update(
                """
                        DELETE FROM tasks WHERE id = ?
                        """, id);
        return rowsAffected > 0;
    }

    @Override
    public TaskResultDTO createTask(TaskCreateDTO taskCreateDTO) {
        SqlRowSet rs = jdbcTemplate.queryForRowSet(
                """
                        INSERT INTO tasks(title,desc,created_at,updated_at,status)
                        VALUES (?,?,?,?,?)
                        """, taskCreateDTO.title(),taskCreateDTO.desc(),
                taskCreateDTO.created_at(),taskCreateDTO.updated_at(),taskCreateDTO.status());
        return new TaskResultDTO(rs.getInt("id"),taskCreateDTO.title(),taskCreateDTO.desc(),
                taskCreateDTO.created_at(),taskCreateDTO.updated_at(),taskCreateDTO.status());
    }

    @Override
    public TaskResultDTO updateTask(TaskUpdateDTO taskUpdateDTO) {
        return null;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
