--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11
-- Dumped by pg_dump version 15.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: abbtech
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO abbtech;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: abbtech
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: task_status; Type: TYPE; Schema: public; Owner: abbtech
--

CREATE TYPE public.task_status AS ENUM (
    'processing',
    'completed',
    'deleted'
);


ALTER TYPE public.task_status OWNER TO abbtech;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.tasks (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    "desc" character varying(255) NOT NULL,
    created_at timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_at timestamp(6) without time zone DEFAULT now() NOT NULL,
    status public.task_status DEFAULT 'processing'::public.task_status NOT NULL
);


ALTER TABLE public.tasks OWNER TO abbtech;

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.tasks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO abbtech;

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.tasks (id, title, "desc", created_at, updated_at, status) FROM stdin;
4	Test Title	Test Desc	2024-04-10 03:14:26.753592	2024-04-10 03:14:26.753592	processing
5	Test Title	Test Desc	2024-04-10 03:18:09.798912	2024-04-10 03:18:09.798912	processing
6	Test Title	Test Desc	2024-04-10 03:23:21.344013	2024-04-10 03:23:21.34502	processing
7	Test Title	Test Desc	2024-04-10 03:23:22.345405	2024-04-10 03:23:22.345405	processing
8	Test Title	Test Desc	2024-04-10 03:23:23.213349	2024-04-10 03:23:23.213349	processing
9	Test Title	Test Desc	2024-04-10 03:33:49.490381	2024-04-10 03:33:49.490381	processing
10	Test Title	Test Desc	2024-04-10 03:40:56.047622	2024-04-10 03:40:56.047622	processing
11	Updated Test Title	Updated Test Title	2024-04-10 03:41:01.060721	2024-04-10 00:45:20.336462	completed
\.


--
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.tasks_id_seq', 11, true);


--
-- Name: tasks tasks_pk; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pk PRIMARY KEY (id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: abbtech
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

